# internations test

`yarn start` will run this application locally.

Choices:

* For simplicity of config, [create-react-app](//npmjs.org/package/create-react-app)
* For separation of logic from view concerns: [recompose](//npmjs.org/package/recompose)
* To utilize the BEM notation pattern: [blem](//npmjs.org/package/blem)
  - this is a module I have published and maintain.
* For composed functions and general toolbelt utilities (generally comparable to [ramda](//npmjs.org/package/ramda)): [f-utility](//npmjs.org/package/f-utility)
  - this is a module I have published and maintain.
* For straightforward logging as a side-effect: [xtrace](//npmjs.org/package/xtrace)
  - this is a module I have published and maintain.


Approach:

I started with the default `create-react-app` configuration, for broad ease-of-local-development. Next I added `node-sass` so I could utilize SCSS, as it remains my strongest styling language generally. Using the BEM notation pattern, I mocked up a rough component / element outline:

```scss
.App {
  &__header {}
  &__title {
    &--main {}
    &--groups {}
    &--users {}
  }
  &__group-listing {}
  &__user-listing {}
}
```

Then, as I started to have multiple component definitions, I extracted and set up new individual exports, e.g. `User.js` / `Groups.js`.

I tried to use recompose to separate logic from the view layer -- I ran into trouble as I was editing because I didn't commit enough.

At the moment, when creating a new user the user is added to all groups -- I see the data moving across but right now it looks like the default values passed to User.js aren't correct.

I would spend some more time working on that except that I am now an hour over the time limit.

Additionally I built an alternative version here which has better data architecture: [https://gitlab.com/brekk/internations-v2](https://gitlab.com/brekk/internations-v2)
