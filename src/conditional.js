import React from "react"
import { isFunction } from "f-utility"

// declarative {x && <jsx>}
export const If = ({ condition, children }) =>
  isFunction(condition) ? condition() : condition ? <>{children}</> : null

// declarative {x ? <a/> : <b/>}
export const Case = ({ condition, children }) =>
  isFunction(condition) ? (
    condition()
  ) : condition ? (
    <>{children[1]}</>
  ) : (
    <>{children[0]}</>
  )
