import React from "react"
import ReactDOM from "react-dom"
import blem from "blem"
import User, { Name, Description, UserGroups, onClickAddGroup } from "./User"

it("User renders without crashing", () => {
  const div = document.createElement("div")
  ReactDOM.render(<User />, div)
  ReactDOM.unmountComponentAtNode(div)
})
it("Name renders without crashing", () => {
  const div = document.createElement("div")
  ReactDOM.render(<Name bem={blem("Name")} />, div)
  ReactDOM.unmountComponentAtNode(div)
})
it("Name renders without crashing", () => {
  const div = document.createElement("div")
  ReactDOM.render(<Name bem={blem("Name")} editable={true} />, div)
  ReactDOM.unmountComponentAtNode(div)
})
it("UserGroups renders without crashing", () => {
  const div = document.createElement("div")
  ReactDOM.render(
    <UserGroups
      editable={true}
      bem={blem("UserGroups")}
      userGroups={`abc`.split(``)}
    />,
    div
  )
  ReactDOM.unmountComponentAtNode(div)
})

it("Description renders without crashing", () => {
  const div = document.createElement("div")
  ReactDOM.render(
    <Description editable={true} bem={blem("Description")} />,
    div
  )
  ReactDOM.unmountComponentAtNode(div)
})

test("onClickAddGroup", done => {
  onClickAddGroup([`test`], {
    addGroup: () => {},
    potentialGroup: false,
    setPotentialGroup: t => {
      expect(t).toEqual(`test`)
      done()
    },
    userGroups: []
  })()
})
