import React from "react"
import { pathOr, pipe, map } from "f-utility"
import { withState } from "recompose"
import { trace } from "xtrace"

import { Option } from "./User"
import { If } from "./conditional"

export const decorate = pipe(
  withState("username", "setUsername", ""),
  withState("firstGroup", "setFirstGroup", ""),
  withState("userGroups", "setUserGroup", "")
)

export const Component = ({
  setUser,
  bem,
  groups,
  username,
  setUsername,
  addUser,
  setFirstGroup,
  firstGroup = ``
}) => (
  <div className={bem("add-new-user")}>
    <h3 className={bem("title", "add-new-user")}>Add new user</h3>
    <input
      type="text"
      className={bem("input", "add-user")}
      value={username}
      onChange={pipe(
        pathOr("", ["target", "value"]),
        setUsername
      )}
    />
    <If condition={groups && groups.length > 0}>
      <>
        <h4 className={bem("title", "add-user-with-groups")}>With groups</h4>
        <select
          className={bem("choose-groups")}
          onChange={pipe(
            pathOr("", ["target", "value"]),
            setFirstGroup
          )}
        >
          {groups && map(Option, ["-"].concat(groups))}
        </select>
      </>
    </If>
    <button
      onClick={() => {
        console.log(firstGroup, groups, username, `pious`)
        const user = { username }
        if (firstGroup && firstGroup !== "" && firstGroup !== "-") {
          user.group = firstGroup
        }
        console.log("user to add", user)
        addUser(user)
        setUsername("")
        setFirstGroup("")
      }}
      className={bem("button", "add-user")}
    >
      Add new user
    </button>
  </div>
)

export default decorate(Component)
