import React from "react"
import ReactDOM from "react-dom"
import { If, Case } from "./conditional"

const render = jsx => {
  const div = document.createElement("div")
  ReactDOM.render(jsx, div)
  ReactDOM.unmountComponentAtNode(div)
}
test("If(true) renders without crashing", () => {
  render(<If condition={true}>{`sure`}</If>)
})

test("If(fn()) renders without crashing", () => {
  render(<If condition={() => true}>{`sure sure`}</If>)
})

test("Case(true) renders without crashing", () => {
  render(<Case condition={true}>{`sure`}</Case>)
})

test("Case(fn()) renders without crashing", () => {
  render(<Case condition={() => true}>{["bad", "good"]}</Case>)
})
test("Case(false) renders without crashing", () => {
  render(<Case condition={false}>{["bad", "good"]}</Case>)
})
