import React, { Fragment } from "react"
import { curry, pipe, map, pathOr, propOr, filter, I } from "f-utility"
import blem from "blem"
import { withHandlers, withState } from "recompose"
import { trace } from "xtrace"

import { Case, If } from "./conditional"
import { targval } from "./utils"

import "./User.scss"

export const decorate = pipe(
  withHandlers({
    toggleEditable: ({ setEditable, editable }) => () => setEditable(!editable),
    removeGroup: ({ setUserGroups, userGroups }) => group =>
      setUserGroups(userGroups.filter(g => g !== group)),
    addGroup: ({
      name,
      addUserToGroup,
      setUserGroups,
      userGroups
    }) => group => {
      addUserToGroup({ user: name, group })
      setUserGroups(userGroups.concat(group))
    }
  }),
  withState("userGroups", "setUserGroups", propOr([], "groups")),
  withState("potentialGroup", "setPotentialGroup", ""),
  withState("editable", "setEditable", false),
  withState("name", "setName", ({ name }) => name || `new user`),
  withState("description", "setDescription", "")
)

export const Name = ({ editable, bem, name, setName }) => (
  <Case condition={editable}>
    {[
      <h2 key="header" className={bem("name")}>
        {name}
      </h2>,
      <Fragment key="edit-mode">
        <strong className={bem("name-label")}>Name</strong>
        <input
          className={bem("name", "editable")}
          key="name"
          type="text"
          defaultValue={name}
          onChange={pipe(
            pathOr(name, ["target", "value"]),
            setName
          )}
        />
      </Fragment>
    ]}
  </Case>
)

export const Description = ({ editable, description, setDescription, bem }) => (
  <Case condition={editable}>
    {[
      <p key="text" className={bem("description")}>
        {description}
      </p>,
      <Fragment key="edit-mode">
        <strong className={bem("description-label")}>Description</strong>
        <textarea
          key="textarea"
          className={bem("description", "editable")}
          onChange={pipe(
            targval(description),
            setDescription
          )}
          value={description}
        />
      </Fragment>
    ]}
  </Case>
)

export const onClickAddGroup = curry(
  (
    filteredGroups,
    { addGroup, potentialGroup, userGroups, setPotentialGroup }
  ) => () => {
    if (potentialGroup && userGroups.indexOf(potentialGroup) === -1)
      addGroup(potentialGroup)
    setPotentialGroup(filteredGroups[0])
  }
)

export const Option = x => (
  <option value={x} key={x}>
    {x}
  </option>
)

export const UserGroups = ({
  groups,
  userGroups,
  setUserGroups,
  bem,
  editable,
  removeGroup,
  addGroup,
  potentialGroup,
  setPotentialGroup
}) => {
  const filteredGroups =
    (groups && groups.filter(g => userGroups.indexOf(g) === -1)) || []
  console.log(groups, "@!", userGroups, `USER GROUPS`, filteredGroups)
  return (
    <If condition={editable || (userGroups && userGroups.length > 0)}>
      <>
        {userGroups &&
          userGroups.length && (
            <strong className={bem("groups-label")}>User Groups</strong>
          )}
        <ul className={bem("groups")}>
          {pipe(
            filter(I),
            map(group => (
              <li key={group} className={bem("group")}>
                {group}
                <If condition={editable}>
                  <button
                    className={bem("button", "remove")}
                    onClick={() => removeGroup(group)}
                  >
                    &times;
                  </button>
                </If>
              </li>
            ))
          )(userGroups)}
        </ul>
        <If condition={editable}>
          <Case condition={filteredGroups.length > 0}>
            {[
              <Fragment key="no-new">No new groups to add!</Fragment>,
              <Fragment key="edit-mode">
                <strong>Add group</strong>
                <select
                  className={bem("add-group")}
                  value={potentialGroup}
                  onChange={pipe(
                    pathOr(``, ["target", "value"]),
                    setPotentialGroup
                  )}
                >
                  {map(Option, filteredGroups)}
                </select>
                <button
                  className={bem("button", "add-group")}
                  onClick={onClickAddGroup(filteredGroups, {
                    addGroup,
                    potentialGroup,
                    userGroups,
                    setPotentialGroup
                  })}
                >
                  Add
                </button>
              </Fragment>
            ]}
          </Case>
        </If>
      </>
    </If>
  )
}

export const User = ({
  name,
  bem = blem("User"),
  description,
  // from HOC
  addGroup,
  userGroups,
  groups,
  editable,
  setEditable,
  setDescription,
  setName,
  removeUser,
  setUserGroups,
  removeGroup,
  potentialGroup,
  setPotentialGroup
}) => (
  <div className={bem()}>
    <button
      className={bem("toggle-editable")}
      onClick={() => setEditable(!editable)}
    >
      {editable ? `Save` : `Edit`}
    </button>
    <Name name={name} bem={bem} editable={editable} setName={setName} />
    <Description
      bem={bem}
      description={description}
      editable={editable}
      setDescription={setDescription}
    />
    <UserGroups
      bem={bem}
      editable={editable}
      groups={groups}
      userGroups={userGroups}
      setUserGroups={setUserGroups}
      removeGroup={removeGroup}
      editable={editable}
      potentialGroup={potentialGroup}
      setPotentialGroup={setPotentialGroup}
      addGroup={addGroup}
    />
    <If condition={editable}>
      <button
        className={bem("button", "delete")}
        onClick={() => removeUser(name)}
      >
        Delete this user
      </button>
    </If>
  </div>
)

export default decorate(User)
