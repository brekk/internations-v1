import React from "react"
import blem from "blem"
import { propOr, pathOr, path, pipe, map } from "f-utility"
import { withHandlers, withState } from "recompose"
import { trace } from "xtrace"

import { If } from "./conditional"
import { targval } from "./utils"

import "./Groups.scss"

export const decorate = pipe(
  withHandlers({
    addGroup: ({ setGroups, groups }) => group => {
      const newGroups = groups.concat(group)
      setGroups(newGroups)
    },
    removeGroup: ({ setError, groupHasUser, setGroups, groups }) => group => {
      const newGroups = groups.filter(g => g !== group)
      if (!groupHasUser(group)) {
        setGroups(newGroups)
      } else {
        setError("Unable to remove group, users are still in it.")
      }
    }
  }),
  withState("input", "setInput", "")
)

export const Group = ({ group, bem, removeGroup }) => (
  <>
    <li className={bem("group")}>
      {group}
      <button
        className={bem("button", "remove-group")}
        onClick={() => removeGroup(group)}
      >
        &times;
      </button>
    </li>
  </>
)

export const Component = ({
  bem = blem("Groups"),
  // from hoc
  groups,
  setInput,
  addGroup,
  removeGroup,
  input,
  error
}) => (
  <div className={bem()}>
    {error && <strong className={bem("error")}>{error}</strong>}
    <input
      className={bem("new-group")}
      type="text"
      value={input}
      onChange={pipe(
        pathOr("", ["target", "value"]),
        setInput
      )}
    />
    <button
      className={bem("button", "add-group")}
      onClick={() => {
        if (input.trim() !== "") addGroup(input)
        setInput("")
      }}
    >
      Add new group
    </button>
    <If condition={groups}>
      <ul className={bem("groups")}>
        {groups &&
          map(
            x => (
              <Group key={x} group={x} bem={bem} removeGroup={removeGroup} />
            ),
            groups
          )}
      </ul>
    </If>
  </div>
)

export default decorate(Component)
