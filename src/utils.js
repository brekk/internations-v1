import { pathOr, $ } from "f-utility"

export const targval = pathOr($, [`target`, `value`], $)
