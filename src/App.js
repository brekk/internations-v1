import React from "react"
import blem from "blem"
import { withHandlers, withState } from "recompose"
import { merge, propOr, map, isFunction, pipe, pathOr } from "f-utility"
import { trace } from "xtrace"
import User from "./User"
import Groups from "./Groups"
import AddNewUser from "./AddNewUser"

import "./App.scss"

export const decorate = pipe(
  withHandlers({
    addUser: props => ({ username, group }) => {
      const found = props.users.filter(u => u.name === username).length > 0
      if (found) return
      const newUsers = [
        {
          name: username,
          userGroups: [group]
        }
      ].concat(props.users)

      props.setUsers(newUsers)
    },
    groupHasUser: ({ groupUserMap }) => group =>
      groupUserMap && groupUserMap[group] && groupUserMap[group].length > 0,
    removeUser: ({ users, setUsers }) => username => {
      setUsers(users.filter(u => u.name !== username))
    },
    addUserToGroup: ({ groupUserMap, setGroupUserMap }) => ({ user, group }) =>
      pipe(
        merge(groupUserMap),
        setGroupUserMap
      )({ [group]: (groupUserMap[group] || []).concat(user) })
  }),
  withState("error", "setError", false),
  withState("groupUserMap", "setGroupUserMap", {}),
  withState("groups", "setGroups", []),
  withState("users", "setUsers", [
    {
      name: "new user",
      description: ``
    },
    { name: "other", description: "other description" }
  ])
)

export const Component = ({
  groups,
  setGroups,
  bem = blem("App"),
  users,
  deleteGroup,
  addUserToGroup,
  groupHasUser,
  setError,
  error,
  addUser,
  removeUser
}) => {
  return (
    <div className={bem()}>
      <header className={bem("header")}>
        <h1 className={bem("title", "main")}>User CMS</h1>
      </header>
      <section className={bem("group-listing")}>
        <h2 className={bem("title", "groups")}>Groups</h2>
        <Groups
          groups={groups}
          setGroups={setGroups}
          groupHasUser={groupHasUser}
          setError={setError}
          error={error}
        />
      </section>
      <section className={bem("user-listing")}>
        <h2 className={bem("title", "users")}>Users</h2>
        <AddNewUser bem={bem} groups={groups} addUser={addUser} />
        {map(
          x => (
            <User
              {...x}
              key={x.name}
              groups={groups}
              userGroups={x.userGroups}
              removeUser={removeUser}
              addUserToGroup={addUserToGroup}
            />
          ),
          users
        )}
      </section>
    </div>
  )
}

export default decorate(Component)
